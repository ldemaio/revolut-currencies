package it.twospecials.revolutcurrencies.data.deserializers

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import it.twospecials.revolutcurrencies.data.CurrenciesResponse
import it.twospecials.revolutcurrencies.data.Currency
import java.lang.reflect.Type

class CurrenciesResponseDeserializer : JsonDeserializer<CurrenciesResponse> {

    override fun deserialize(
        json: JsonElement?,
        typeOfT: Type?,
        context: JsonDeserializationContext?
    ): CurrenciesResponse {
        val ratesObject = json?.asJsonObject?.getAsJsonObject("rates")
        val values = ArrayList<Currency>()
        for (key in ratesObject!!.keySet()){
            val value = ratesObject[key]
            values.add(Currency(key,value.asFloat))
        }
        val base = json?.asJsonObject?.getAsJsonPrimitive("base")!!.asString
        return CurrenciesResponse(values, Currency(base,1f))
    }
}