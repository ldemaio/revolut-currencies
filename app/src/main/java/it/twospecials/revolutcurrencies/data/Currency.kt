package it.twospecials.revolutcurrencies.data

data class Currency(var name:String, var value: Float)