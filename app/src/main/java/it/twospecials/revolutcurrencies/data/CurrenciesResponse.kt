package it.twospecials.revolutcurrencies.data

data class CurrenciesResponse(val currencies: ArrayList<Currency>, val base: Currency)