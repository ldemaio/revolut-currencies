package it.twospecials.revolutcurrencies

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import it.twospecials.revolutcurrencies.pages.views.CurrenciesFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportFragmentManager.beginTransaction().add(R.id.main_frame, CurrenciesFragment()).commit()
    }
}
