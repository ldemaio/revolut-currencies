package it.twospecials.revolutcurrencies.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import it.twospecials.revolutcurrencies.R
import it.twospecials.revolutcurrencies.data.Currency
import it.twospecials.revolutcurrencies.databinding.CurrencyViewModel
import it.twospecials.revolutcurrencies.databinding.RowCurrencyBinding

class CurrenciesRecyclerAdapter(private val itemClickListener: OnItemClickListener, val lifecycleOwner: LifecycleOwner):
    RecyclerView.Adapter<CurrenciesRecyclerAdapter.CurrenciesViewHolder>() {

    private var list: List<MutableLiveData<Currency>>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrenciesViewHolder {
        val databindingInflate = DataBindingUtil.inflate<RowCurrencyBinding>(
            LayoutInflater.from(parent.context),
            R.layout.row_currency,
            parent,
            false
        )
        return CurrenciesViewHolder(databindingInflate,lifecycleOwner)
    }

    override fun getItemCount(): Int {
        return list?.size ?: 0
    }

    override fun onBindViewHolder(holder: CurrenciesViewHolder, position: Int) {
        var item =  list?.get(position)!!
        holder.bindView(item,onItemValueChanged)
        holder.bindOnItemClickListener(View.OnClickListener {
            itemClickListener?.onItemClicked(item!!)
        })

    }

    class CurrenciesViewHolder(private val itemBinder: RowCurrencyBinding, private val lifecycleOwner:LifecycleOwner):RecyclerView.ViewHolder(itemBinder.root){
        fun bindView(currency:MutableLiveData<Currency>,onItemValueChanged:OnItemValueChanged){
            itemBinder.data = CurrencyViewModel(currency, lifecycleOwner,onItemValueChanged)
            itemBinder.executePendingBindings()
        }
        fun bindOnItemClickListener(onClickListener: View.OnClickListener) {
            itemBinder.root.setOnClickListener(onClickListener)
        }
    }

    fun updateData(list: List<MutableLiveData<Currency>>) {
        this.list = list
        notifyDataSetChanged()
    }

    interface OnItemClickListener {
        fun onItemClicked(field: MutableLiveData<Currency>)
    }

    lateinit var onItemValueChanged:OnItemValueChanged

    interface OnItemValueChanged{
        fun onItemValueChanged(field: Currency)
    }

}