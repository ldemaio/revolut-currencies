package it.twospecials.revolutcurrencies.pages.presenters

import android.annotation.SuppressLint
import android.os.Handler
import android.os.Looper
import androidx.lifecycle.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import it.twospecials.revolutcurrencies.data.Currency
import it.twospecials.revolutcurrencies.pages.contracts.CurrenciesContractInterface
import it.twospecials.revolutcurrencies.repositories.CurrenciesRepository

class CurrenciesPresenter(private var currenciesRepository: CurrenciesRepository): CurrenciesContractInterface.Presenter,
    LifecycleObserver {

    override fun itemValueChanged(field: Currency) {
        currentQuantitySelected = field.value
        calculateUserRequestedValue()
    }

    private val updateTextTask = object : Runnable {
        override fun run() {
            loadData()
            mainHandler.postDelayed(this, 1000)
        }
    }

    private lateinit var mainHandler:Handler

    override fun attach(view: CurrenciesContractInterface.View, viewLifecycle: Lifecycle) {
        this.view = view

        mainHandler = Handler(Looper.getMainLooper())
        mainHandler.post(updateTextTask)

        viewLifecycle.addObserver(this)
    }

    private lateinit var view: CurrenciesContractInterface.View

    private var currencies = ArrayList<Currency>()
    private var valuesMap = HashMap<String,Float>()

    private var currentQuantitySelected: Float = 1f
    private var currencySelected: String = "EUR"

    override fun onCurrencySelected(currency: Currency) {
        currentQuantitySelected = currency.value
        mainHandler.removeCallbacks(updateTextTask)
        mainHandler.post(updateTextTask)
        this.currencySelected = currency.name
    }

    private val currenciesLiveData = MutableLiveData<ArrayList<MutableLiveData<Currency>>>()

    fun getCurrencies():LiveData<ArrayList<MutableLiveData<Currency>>>{
        return currenciesLiveData
    }

    @SuppressLint("CheckResult")
    private fun loadData(){
        currenciesRepository.getCurrencies(currencySelected).observeOn(AndroidSchedulers.mainThread()).subscribe { data->
            var list = ArrayList<MutableLiveData<Currency>>()
            if (currencies.isEmpty()) {
                var currencyLiveData = MutableLiveData<Currency>()
                currencyLiveData.value = data.base
                list.add(currencyLiveData)
                this.currencies.add(data.base)
                for (currency in data.currencies) {
                    var currencyLiveData = MutableLiveData<Currency>()
                    currencyLiveData.value = currency
                    this.currencies.add(currency)
                    list.add(currencyLiveData)
                }
                currenciesLiveData.value = list
            }else{
                for (newCurrency in data.currencies){
                    valuesMap[newCurrency.name] = newCurrency.value
                    for (liveData in currenciesLiveData.value!!){
                        if (liveData.value!!.name==newCurrency.name){
                            newCurrency.value *= currentQuantitySelected
                            liveData.value = newCurrency
                        }
                    }
                }
            }
        }
    }

    private fun calculateUserRequestedValue(){
        for (liveData in currenciesLiveData.value!!) {
            if (currencySelected!=liveData.value!!.name){
                liveData.value = Currency(liveData.value!!.name, valuesMap[liveData.value!!.name]!!*currentQuantitySelected)
            }
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    private fun onViewDestroyed() {
        mainHandler.removeCallbacks(updateTextTask)
    }
}