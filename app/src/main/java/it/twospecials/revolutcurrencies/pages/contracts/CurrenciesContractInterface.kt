package it.twospecials.revolutcurrencies.pages.contracts

import androidx.lifecycle.Lifecycle
import it.twospecials.revolutcurrencies.data.Currency

interface CurrenciesContractInterface {

    interface View

    interface Presenter{
        fun attach(view:View,viewLifecycle: Lifecycle)
        fun onCurrencySelected(currency: Currency)
        fun itemValueChanged(field:Currency)
    }
}