package it.twospecials.revolutcurrencies.pages.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import it.twospecials.revolutcurrencies.R
import it.twospecials.revolutcurrencies.adapters.CurrenciesRecyclerAdapter
import it.twospecials.revolutcurrencies.data.Currency
import it.twospecials.revolutcurrencies.injection.components.DaggerCurrenciesComponent
import it.twospecials.revolutcurrencies.injection.module.CurrenciesModule
import it.twospecials.revolutcurrencies.pages.contracts.CurrenciesContractInterface
import it.twospecials.revolutcurrencies.pages.presenters.CurrenciesPresenter
import kotlinx.android.synthetic.main.fragment_currencies.*
import javax.inject.Inject

class CurrenciesFragment:Fragment(), CurrenciesContractInterface.View,
    CurrenciesRecyclerAdapter.OnItemClickListener {

    override fun onItemClicked(field: MutableLiveData<Currency>) {
        currenciesRecyclerView.scrollToPosition(0)
        adapter.notifyItemMoved(currencies.indexOf(field),0)

        currencies.remove(field)
        currencies.add(0,field)

        currenciesPresenter.onCurrencySelected(field.value!!)
    }

    private lateinit var currencies: ArrayList<MutableLiveData<Currency>>

    @Inject
    lateinit var currenciesPresenter: CurrenciesPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectDependencies()

        currenciesPresenter.attach(this, lifecycle)

        currenciesPresenter.getCurrencies().observe(this, Observer {
            currenciesLoadingProgressBar.visibility = View.GONE
            currenciesRecyclerView.visibility = View.VISIBLE
            currencies = it
            adapter.updateData(currencies)
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_currencies,container,false)
    }

    lateinit var adapter: CurrenciesRecyclerAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = CurrenciesRecyclerAdapter(this, this)
        adapter.onItemValueChanged = object : CurrenciesRecyclerAdapter.OnItemValueChanged{
            override fun onItemValueChanged(field: Currency) {
                currenciesPresenter.itemValueChanged(field)
            }
        }
        currenciesRecyclerView.layoutManager = LinearLayoutManager(requireContext())

        currenciesRecyclerView.adapter = adapter
    }

    private fun injectDependencies(){
        val currenciesComponent = DaggerCurrenciesComponent.builder().currenciesModule(CurrenciesModule()).build()
        currenciesComponent.inject(this)
    }
}