package it.twospecials.revolutcurrencies.repositories

import io.reactivex.Observable
import it.twospecials.revolutcurrencies.data.CurrenciesResponse

class CurrenciesRepository(val api: Apis) {
    fun getCurrencies(base:String): Observable<CurrenciesResponse> {
        return api.getCurrencies(base)
    }
}