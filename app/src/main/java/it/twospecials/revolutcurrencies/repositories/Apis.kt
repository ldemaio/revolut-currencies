package it.twospecials.revolutcurrencies.repositories

import io.reactivex.Observable
import it.twospecials.revolutcurrencies.data.CurrenciesResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface Apis {
    @GET("/latest")
    fun getCurrencies(@Query("base") base: String): Observable<CurrenciesResponse>
}