package it.twospecials.revolutcurrencies.databinding

import android.widget.ImageView
import androidx.databinding.BindingAdapter


class CurrencyBindAdapter() {

    companion object{
        @JvmStatic
        @BindingAdapter("android:src")
        fun setImageResource(imageView: ImageView, resource: Int) {
            imageView.setImageResource(resource)
        }
    }
}