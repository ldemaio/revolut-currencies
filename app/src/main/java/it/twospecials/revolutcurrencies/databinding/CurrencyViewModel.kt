package it.twospecials.revolutcurrencies.databinding

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import it.twospecials.revolutcurrencies.adapters.CurrenciesRecyclerAdapter
import it.twospecials.revolutcurrencies.data.Currency


class CurrencyViewModel(private val currencyLiveData: LiveData<Currency>, lifeCycle: LifecycleOwner, var onItemValueChanged: CurrenciesRecyclerAdapter.OnItemValueChanged):BaseObservable() {
    private lateinit var currency: Currency
    init {
        currencyLiveData.observe(lifeCycle, Observer { data->
            currency = data
            notifyChange()
        })
    }

    @Bindable
    fun getValue():String{
        return "%.2f".format(currency.value)
    }

    fun setValue(value:String){
        if ("%.2f".format(currency.value)!=value) {
            if (!value.isEmpty())
                currencyLiveData.value!!.value = value.toFloat()
            else
                currencyLiveData.value!!.value = 0f
            onItemValueChanged?.onItemValueChanged(currency)
        }
    }

    @Bindable
    fun getImageResource():Int{
        val resId = getResId("flag_"+currency.name.toLowerCase(), it.twospecials.revolutcurrencies.R.drawable::class.java)
        return resId
    }

    @Bindable
    fun getName():String{
        return currency.name
    }

    @Bindable
    fun getSymbol():String{
        return java.util.Currency.getInstance(currency.name).displayName
    }

    private fun getResId(resName: String, c: Class<*>): Int {
        return try {
            val idField = c.getDeclaredField(resName)
            idField.getInt(idField)
        } catch (e: Exception) {
            e.printStackTrace()
            -1
        }
    }
}