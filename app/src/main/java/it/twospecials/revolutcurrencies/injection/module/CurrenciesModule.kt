package it.twospecials.revolutcurrencies.injection.module

import android.content.SharedPreferences
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.Reusable
import io.reactivex.schedulers.Schedulers
import it.twospecials.revolutcurrencies.data.CurrenciesResponse
import it.twospecials.revolutcurrencies.data.deserializers.CurrenciesResponseDeserializer
import it.twospecials.revolutcurrencies.pages.presenters.CurrenciesPresenter
import it.twospecials.revolutcurrencies.repositories.Apis
import it.twospecials.revolutcurrencies.repositories.CurrenciesRepository
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import javax.inject.Singleton

@Module
class CurrenciesModule {

    @Provides
    fun provideCurrenciesRepository(api: Apis):CurrenciesRepository{
        return CurrenciesRepository(api)
    }

    @Provides
    @Reusable
    internal fun provideApis(retrofit: Retrofit): Apis {
        return retrofit.create(Apis::class.java)
    }

    @Provides
    @Reusable
    internal fun provideRetrofitInterface(
        gson: GsonConverterFactory,
        okHttpClient: OkHttpClient
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl("https://revolut.duckdns.org/") //base url is injected by okHttpClient
            .client(okHttpClient)
            .addConverterFactory(gson)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .build()
    }

    @Singleton
    @Provides
    fun provideOkHttpClient( ) =
        OkHttpClient.Builder()
            .addInterceptor {
                val request = it.request()
                Timber.d("Request: "+request.url())
                it.proceed(request)
            }
            .build()

    @Singleton
    @Provides
    fun provideGson(gson:Gson)= GsonConverterFactory.create(gson)

    @Singleton
    @Provides
    fun providesGson(): Gson {
        return GsonBuilder()
            .registerTypeAdapter(CurrenciesResponse::class.java, CurrenciesResponseDeserializer())
            .create()
    }

    @Provides
    fun providesCurrenciesPresenter(currenciesRepository:CurrenciesRepository) = CurrenciesPresenter(currenciesRepository)

}