package it.twospecials.revolutcurrencies.injection.components

import dagger.Component
import it.twospecials.revolutcurrencies.injection.module.CurrenciesModule
import it.twospecials.revolutcurrencies.pages.views.CurrenciesFragment
import javax.inject.Singleton

@Singleton
@Component(modules = [CurrenciesModule::class])
interface CurrenciesComponent {
    fun inject(app: CurrenciesFragment)
}